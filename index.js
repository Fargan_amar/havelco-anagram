'use strict'

const readLine = require('readline');

const ui = readLine.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
})

const arrOfWords = [];
let result = 'NO';

const main = () => {
(    ui.question(`Welcome to the Anagram, Please type 'start' for play the game or type 'exit' to exit from this game : `, (data) => {
        if (data.toLowerCase() !== 'start') {
            console.log('Have a Nice Day, BYE!')
            process.exit();
        }

        start();
    }))
}


const txt1 = () => {
    return new Promise((resolve, reject) => {
        ui.question('Type Word 1 : ', data => {
            arrOfWords.push(data.replace(/[^\w]/g, "").toLowerCase())
            resolve()
        })
    })
}

const txt2 = () => {
    return new Promise((resolve, reject) => {
        ui.question('Type Word 2 : ', data => {
            arrOfWords.push(data.replace(/[^\w]/g, "").toLowerCase())
            resolve()
        })
    })
}

const isAnagram = (arrWords) => {
    return new Promise(async(resolve, reject) => {
        const word1 = await getAllCharacter(arrWords[0])
        const word2 = await getAllCharacter(arrWords[1])

        
        for(let i in word1) {
            if (word1[i] !== word2[i]) {
                resolve(result)
            }

        }
        
        result = 'YES'
        
        resolve(result)
    })
}


const getAllCharacter = (word) => {
    return new Promise((resolve, reject) => {
        let charMap = {}
    
        for (let char of word) {
            charMap[char] = charMap[char] + 1 || 1
        }
        
        resolve(charMap);

    })
}

const repeatMessage = () => {
    return new Promise((resolve, reject) => {
        ui.question(`Please type 'restart' for return play the game or type 'exit' to exit from this game : `, (data) => {
            if (data.toLowerCase() !== 'restart') {
                console.log('Have a Nice Day, BYE!')
                process.exit();
            }
    
            start();
            resolve()
        })        
    })
}

const start = async() => {
    await txt1()
    await txt2()

    const answer = await isAnagram(arrOfWords)

    console.log(answer, '\n \n')

    await repeatMessage()
}


main();